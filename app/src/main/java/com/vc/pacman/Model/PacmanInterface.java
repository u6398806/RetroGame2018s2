package com.vc.pacman.Model;

/**
 * Created by chaahatjain on 16/9/18.
 * For observer design pattern. Pacman is visible to other ghosts
 */

public interface PacmanInterface {
    /**
     * When pacman touches a ghost, then this method is called to reset pacman location to home
     */
    void reset();

    /**
     * Used to send the current position of pacman to the ghost so that they can apply algorithm to approach it.
     */
    void sendPosition();

    /**
     * Send the direction Pacman is currently facing.
     */
    Direction sendDirectionFacing();

    /**
     * Add an enemy that observes pacman
     * @param enemy
     */
    void addEnemy(EnemyFunctions enemy);

    /**
     * This method goes through all the enemies it is tracking and gives the position of the red enemy.
     * @return
     */
    Position sendRedPosition();

    /**
     * Register this board for playing
     * @param board
     */
    void registerBoard(PlayingBoard board);

    /**
     * Set enemies to frightened mode
     */
    void scareEnemies();
    /**
     * Increase score by 1000 on eating ghost in frightened state
     */
    void boostScore();

}
