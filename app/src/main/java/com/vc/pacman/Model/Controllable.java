package com.vc.pacman.Model;

/**
 * Created by chaahatjain on 16/9/18.
 */

public interface Controllable {
    /**
     * Used to change the direction pacman is currently facing
     * @param direction
     */
    void changeDirectionFacing(Direction direction);
}
