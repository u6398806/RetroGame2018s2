package com.vc.pacman.Model;

/**
 * Created by chaahatjain on 16/9/18.
 */

public interface Movable {
    /**
     * Ghosts and Pacman move in whichever direction decided.
     */
    void move();
}
