package com.vc.pacman.Model.Ghosts;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.vc.pacman.Model.Direction;
import com.vc.pacman.Model.Enemy;
import com.vc.pacman.Model.EnemyState;
import com.vc.pacman.Model.PacmanInterface;
import com.vc.pacman.Model.PlayingBoard;
import com.vc.pacman.Model.Position;

import static com.vc.pacman.Model.Direction.getPosition;

/**
 * Created by chaahatjain on 16/9/18.
 * Ambusher - aims for square right in front of PACMAN
 */

public class Pink extends Enemy {
    private Position targetTile = new Position(0,28);

    @Override
    public Canvas draw(Canvas canvas) {
        Position center = getCenter();
        int radius = getRadius();
        Paint paint = new Paint();
        if (this.getState() != EnemyState.FRIGHTENED) {
            paint.setColor(Color.rgb(240, 151, 156));
        }
        else {
            paint.setColor(Color.BLUE);
        }
        canvas.drawCircle(center.getX(), center.getY(), radius, paint);
        return canvas;
    }

    public Pink(Position home, PacmanInterface pacman, PlayingBoard board) {
        super(home, pacman, board);
    }

    @Override
    public void applyAlgorithm(Position pacmanPosition) {
        // get direction pacman is facing.
        // get next possible move pacman will make.
        // set that as target
        super.applyAlgorithm(pacmanPosition);
        switch (getState()) {
            case CHASE: {
                Direction pacmanFace = getPacmanFacing();
                Position nextPacmanMove = pacmanPosition.add(getPosition(pacmanFace));
                chase(nextPacmanMove);
                break;
            }
            case SCATTER: chase(targetTile);
            case FRIGHTENED: makeRandomMove();
        }
    }
}
