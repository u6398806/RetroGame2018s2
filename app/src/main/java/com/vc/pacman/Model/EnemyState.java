package com.vc.pacman.Model;

/**
 * Created by chaahatjain on 17/9/18.
 * Used to determine whether the enemy motion is currently scattered, to chase Pacman or frigtened of Pacman.
 */

public enum EnemyState {
    FRIGHTENED,SCATTER,CHASE;
}
