package com.vc.pacman.Model;

/**
 * Created by chaahatjain on 17/9/18.
 */

public interface PlayingBoard {
    // Setting the width and the height of the board to a fixed value.
    int WIDTH = 23;
    int HEIGHT = 26;
    int BOARD_START_X = 150;
    int BOARD_START_Y = 550;
    int SQUARE_SIZE = 50;

    /**
     * This method is used to check whether or not it is possible to move to the specified location
     *
     * @param entityPosition
     * @param movement
     * @return
     */
    boolean checkLegalMove(Position entityPosition, Position movement);

    /**
     * Update the board to be none for the given position
     * @param position
     * @return
     */
    Environment updateBoard(Position position);

    /**
     * Convert an index position into coordinate position
     * @param position
     * @return
     */
    Position getPixelLocation(Position position);
}
