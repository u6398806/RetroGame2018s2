package com.vc.pacman.Model;

/**
 * Created by chaahatjain on 17/9/18.
 */

public interface PositionOperations {
    /**
     * Add two position vectors
     * @param position
     * @return
     */
    Position add(Position position);

    /**
     * Give the xCoord of position
     * @return
     */
    int getX();

    /**
     * Give the yCoord of Position
     * @return
     */
    int getY();

    /**
     * Get the distance remaining for this position to reach position.
     * Value returned is a Position vector
     * @param position
     * @return
     */
    Position distanceFrom(Position position);

    /**
     * Give the distance as the mathematical sum of the x and y coordinates.
     * @return
     */
    int getAbsoluteDistance();

    /**
     * Scale a vector.
     * @param scalar
     * @return
     */
    Position scaleBy(int scalar);

    /**
     * Returns the image of this object wrt to the point position.
     * @param position
     * @return
     */
    Position image(Position position);
}
