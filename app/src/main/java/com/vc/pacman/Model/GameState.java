package com.vc.pacman.Model;

/**
 * Created by chaahatjain on 18/10/18.
 * Used to determine the state of the game
 */

public enum GameState {
    WIN,LOSE, PLAYING;
}
