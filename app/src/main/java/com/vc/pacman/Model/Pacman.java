package com.vc.pacman.Model;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.vc.pacman.Model.Ghosts.Red;

import java.util.ArrayList;

/**
 * Created by chaahatjain on 16/9/18.
 * Character controlled by the player
 */

public class Pacman implements PacmanInterface, Controllable, Movable, Drawable {

    private final int HOME_X = 13;
    private final int HOME_Y = 5;
    private final Position HOME = new Position(HOME_X, HOME_Y);
    private ArrayList<EnemyFunctions> enemies = new ArrayList<>();
    private Position position;
    private Direction facing = Direction.LEFT;
    private PlayingBoard myBoard;
    private boolean pellet = false;
    private int numberOfLives = 3;
    private int score;

    @Override
    public void scareEnemies() {
        for (EnemyFunctions enemyFunctions : enemies) {
            enemyFunctions.setState(EnemyState.FRIGHTENED);
        }
    }

    @Override
    public void boostScore() {
        score += 1000;
    }

    public Pacman() {
        this.enemies = new ArrayList<>();
        position = HOME;
        this.score = 0;
    }

    @Override
    public void registerBoard(PlayingBoard board) {
        myBoard = board;
    }

    @Override
    public Position sendRedPosition() {
        for (EnemyFunctions enemyFunctions : enemies) {
            if (enemyFunctions instanceof Red) {
                Red enemy = (Red) enemyFunctions;
                return enemy.position;
            }
        }
        return null;
    }

    @Override
    public void reset() {
        // reset to home
        numberOfLives = numberOfLives- 1; // REMEMBER TO CHANGE LATER
        position = HOME;
        for (EnemyFunctions enemy : enemies) {
            enemy.reset(); // reset all the ghosts to their home
        }
    }

    @Override
    public void sendPosition() {
        for (EnemyFunctions enemy : enemies) {
            enemy.applyAlgorithm(position); // must send the location of pacman instead
        }
    }

    @Override
    public Direction sendDirectionFacing() {
        return facing;
    }

    @Override
    public void changeDirectionFacing(Direction direction) {
        this.facing = direction;
    }

    @Override
    public void move() {
        Position moveIn = Direction.getPosition(facing);

        if (myBoard.checkLegalMove(position,moveIn)) {
            position = position.add(moveIn);
            Environment bonus = myBoard.updateBoard(position);

            switch (bonus) {
                case COIN:
                    score += 100;
                    break;
                case PELLET:
                    pellet = true;
                    scareEnemies();
                    break;
                case FRUIT:
                    score += 500;
                    break;
            }
        }
    }

    @Override
    public void addEnemy(EnemyFunctions enemy) {
        this.enemies.add(enemy);
    }

    public int getNumberOfLives() {
        return numberOfLives;
    }

    public int getScore() {
        return score;
    }

     boolean isPellet() {
        return pellet;
    }

    /**
     * Draw Pacman, Board, Enemies
     * @param canvas
     * @return
     */
    @Override
    public Canvas draw(Canvas canvas) {

        // for the board
        canvas = ((Drawable) myBoard).draw(canvas);

        // for enemies
        for (EnemyFunctions enemyFunctions : enemies) {
            canvas = ((Drawable) enemyFunctions).draw(canvas);
        }

        // Do something to canvas for pacman
        int radius = myBoard.SQUARE_SIZE/2;
        Position center = myBoard.getPixelLocation(position);
        Paint paint = new Paint();
        paint.setColor(Color.rgb(253,255,30));
        canvas.drawCircle(center.getX(), center.getY(), radius, paint);

        Paint textPaint = new Paint();
        textPaint.setTextSize(75);
        textPaint.setColor(Color.WHITE);
        canvas.drawText("CURRENT SCORE : " + score,100,125, textPaint);

        canvas.drawText("NUMBER OF LIVES : " + numberOfLives, 100, 325,textPaint);


        return canvas;
    }
}
