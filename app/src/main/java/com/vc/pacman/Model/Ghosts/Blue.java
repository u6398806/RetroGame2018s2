package com.vc.pacman.Model.Ghosts;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.vc.pacman.Model.Direction;
import com.vc.pacman.Model.Enemy;
import com.vc.pacman.Model.EnemyState;
import com.vc.pacman.Model.PacmanInterface;
import com.vc.pacman.Model.PlayingBoard;
import com.vc.pacman.Model.Position;

import static com.vc.pacman.Model.Direction.getPosition;

/**
 * Created by chaahatjain on 16/9/18.
 * This is a fickle one. Sometimes it decides to go for PACMAN and other times it goes in the opposite direction.
 */

public class Blue extends Enemy {
    private final int SCALAR = 2;
    private Position targetTile = new Position(30,0);
    public Blue( Position home, PacmanInterface pacman, PlayingBoard board) {
        super( home, pacman, board);
    }

    /**
     *
     * @param pacmanPosition
     */
    @Override
    public void applyAlgorithm(Position pacmanPosition) {
        super.applyAlgorithm(pacmanPosition);
        switch (getState()) {
            case CHASE:
                Position redPosition = getRedPosition();
                Direction pacmanFacing = getPacmanFacing();
                Position directionScaled = getPosition(pacmanFacing).scaleBy(SCALAR);
                Position offset = pacmanPosition.add(directionScaled); // Get 2 tiles ahead of pacman.
                Position target = redPosition.image(offset);
                chase(target);
                break;

            case SCATTER:
                chase(targetTile);
                break;

            case FRIGHTENED:
                makeRandomMove();
                break;
        }
    }

    @Override
    public Canvas draw(Canvas canvas) {
        Position center = getCenter();
        int radius = getRadius();
        Paint paint = new Paint();
        if (this.getState() != EnemyState.FRIGHTENED) {
            paint.setColor(Color.rgb(97, 251, 251));
        }
        else {
            paint.setColor(Color.BLUE);
        }
        canvas.drawCircle(center.getX(), center.getY(), radius, paint);
        return canvas;
    }

}
