package com.vc.pacman.Model;

import android.graphics.Canvas;

/**
 * Created by chaahatjain on 28/9/18.
 */

public interface Drawable {
    Canvas draw(Canvas canvas);
}
