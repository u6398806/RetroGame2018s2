package com.vc.pacman.Model;

/**
 * Created by chaahatjain on 16/9/18.
 */

public interface EnemyFunctions {
    /**
     * This method takes in pacman's position and decides which square the ghost shoukd move to
     */
     void applyAlgorithm(Position pacmanPosition);

    /**
     * This method is called to reset the ghost, i.e., move it back to its home
     */
    void reset();

    /**
     * Used to set the state of an enemy to a given state
     * @param state
     */
    void setState(EnemyState state);
}
