package com.vc.pacman.Model.Ghosts;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.vc.pacman.Model.Direction;
import com.vc.pacman.Model.Enemy;
import com.vc.pacman.Model.EnemyState;
import com.vc.pacman.Model.PacmanInterface;
import com.vc.pacman.Model.PlayingBoard;
import com.vc.pacman.Model.Position;

/**
 * Created by chaahatjain on 16/9/18.
 * If at distance from pacman then go for him, otherwise go for the bottom left corner
 */

public class Orange extends Enemy {
     private Position targetTile = new Position(-1,-1);
     public Orange(Position home, PacmanInterface pacman, PlayingBoard board) {
        super(home, pacman, board);
    }

    @Override
    public Canvas draw(Canvas canvas) {
        Position center = getCenter();
        int radius = getRadius();
        Paint paint = new Paint();
        if (this.getState() != EnemyState.FRIGHTENED) {
            paint.setColor(Color.rgb(238, 153, 22));
        }
        else {
            paint.setColor(Color.BLUE);
        }
        canvas.drawCircle(center.getX(), center.getY(), radius, paint);
        return canvas;
    }

    @Override
    public void applyAlgorithm(Position pacmanPosition) {
        super.applyAlgorithm(pacmanPosition);
        switch (getState()) {
            case CHASE:
                Position distance = pacmanPosition.distanceFrom(position);
                int numTiles = distance.getAbsoluteDistance();
                if (numTiles >= 8) {
                    chase(pacmanPosition);
                }
                else {
                    chase(targetTile);
                }
                break;

            case SCATTER:
                chase(targetTile);
                break;

            case FRIGHTENED:
                makeRandomMove();
                break;
        }
    }

}
