package com.vc.pacman.Model;

import android.graphics.Canvas;

import java.util.Random;

/**
 * Created by chaahatjain on 16/9/18.
 */

public class Enemy implements EnemyFunctions, Drawable {
    Position previousPosition;
    public Position position;
    private final Position HOME;
    private PlayingBoard board;
    PacmanInterface pacman; // uses interface to control pacman
    private EnemyState state = EnemyState.CHASE;
    private Position pacmanPosition;
    float startTime,endTime;

    public Enemy(Position home, PacmanInterface pacman, PlayingBoard board) {
        this.position = home;
        this.HOME = home;
        this.pacman = pacman;
        this.board = board;
        previousPosition = null;
    }


    @Override
    public void setState(EnemyState state) {
        if (state == EnemyState.FRIGHTENED) {
            startTime = System.currentTimeMillis();
        }
        this.state = state;
    }

    /**
     * Given a move, change the position accordingly.
     *
     */
    public void move(Direction move) {
        if (state == EnemyState.FRIGHTENED) {
            endTime = System.currentTimeMillis();
            state = endTime - startTime > 4000 ? EnemyState.CHASE : EnemyState.FRIGHTENED;
        }
        previousPosition = position;
        position = position.add(Direction.getPosition(move));
        if (position.equals(pacmanPosition)) {
            if (state == EnemyState.FRIGHTENED) {
                reset();
                pacman.boostScore();
            }
            else {
                pacReset();
            }
        }
    }

    /**
     * This method will be overridden by all of its subclasses
     * @return
     */
    @Override
    public void applyAlgorithm(Position pacmanPosition) {
        this.pacmanPosition = pacmanPosition;
    }

    /**
     * When dead, reset to the home base. To be done in animation so soft rest not hard.
     */
    @Override
    public void reset() {
        // get most efficient way to go home.
        position = HOME;
    }

    /**
     * Call reset on pacman. To do this going to use observer design pattern
     */
    public void pacReset() {
        pacman.reset(); // when this is reset, then reset all enemies as well.
    }

    /**
     * Check whether the given move is legal for the current board.
     * @param move
     * @return
     */
    protected boolean checkLegalMove(Direction move) {
        Position moveDir = Direction.getPosition(move);
        return (!(moveDir.equals(new Position(0,0)))) && board.checkLegalMove(position,moveDir) && !(position.add(moveDir).equals(previousPosition));
    }

    protected Direction getPacmanFacing() {
        return this.pacman.sendDirectionFacing();
    }

    protected EnemyState getState() {
        return this.state;
    }

    /**
     * This method is used to calculate the heuristic value of each move
     * @param pacmanPosition
     * @param move
     * @return
     */
    protected int heuristic(Position pacmanPosition, Direction move) {
        Position moveDir = Direction.getPosition(move);
        Position myFuturePosition = position.add(moveDir);
        Position distance = pacmanPosition.distanceFrom(myFuturePosition);
        return distance.getAbsoluteDistance();
    }

    /**
     * When frightened make a random move. Common to all Enemies
     */
    protected void makeRandomMove() {
        Direction[] directions = Direction.createArray();
        shuffle(directions);
        for (int i = 0; i < directions.length; i++) {
            Direction move = directions[i];
            if (checkLegalMove(move)) {
                move(move);
                break;
            }
        }
    }

    /**
     * Method for shuffling an array implemented in the Fisher-Yates Shuffling style
     * https://stackoverflow.com/questions/1519736/random-shuffling-of-an-array
     * @param directions
     * @return
     */
    private void shuffle(Direction[] directions) {
        Random random = new Random();
        for (int i = directions.length - 1; i > 0; i--) {
            int index = random.nextInt(i + 1);
            Direction temp = directions[index];
            directions[index]  = directions[i];
            directions[i] = temp;
        }
    }

    /**
     * Select best immediate way to reach a target tile. Common to all Enemies
     * @param target
     */
    protected void chase(Position target) {
        Direction[] directions = Direction.createArray();
        Direction bestMove = Direction.NONE;
        int minScore = Integer.MAX_VALUE;

        for (int i = 0; i < directions.length; i++) {
            Direction move = directions[i];
            if (checkLegalMove(move)) {
                int score = heuristic(target, move);
                if (score < minScore) {
                    bestMove = move;
                    minScore = score;
                }

            }
        }
        move(bestMove);
    }

    /**
     * Used to gain access to the position of the red enemy.
     * @return
     */
    protected Position getRedPosition() {
        return pacman.sendRedPosition();
    }

    @Override
    public Canvas draw(Canvas canvas) {
        return canvas;
    }

    protected Position getCenter() {
        return board.getPixelLocation(position);
    }

    protected int getRadius() {
        return board.SQUARE_SIZE/2;
    }
}
