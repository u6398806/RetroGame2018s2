package com.vc.pacman.Model;

/**
 * Created by chaahatjain on 16/9/18.
 * Used to get the index of each object on the board.
 */

public final class Position implements PositionOperations {
    private int x,y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
     @Override
     public int getX() {
        return x;
    }

     @Override
     public int getY() {
        return y;
    }

     @Override
     public Position add(Position move) {
        return new Position(x + move.x, y + move.y);
    }

    @Override
    public Position image(Position position) {
        Position distance = this.distanceFrom(position);
        return position.add(distance);
    }

    @Override
    public Position scaleBy(int scalar) {
        this.x = scalar*x;
        this.y = scalar*y;
        return this;
    }

    @Override
    public int getAbsoluteDistance() {
        return Math.abs(this.x) + Math.abs(this.y);
    }

    @Override
    public Position distanceFrom(Position position) {
        int xDiff = position.x - this.x;
        int yDiff = position.y - this.y;
        return new Position(xDiff,yDiff);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj instanceof Position) {
            Position position = (Position) obj;
            return x == position.x && y == position.y;
        }
        return false;
    }

    @Override
    public String toString() {
        return x + " " + y;
    }
}
