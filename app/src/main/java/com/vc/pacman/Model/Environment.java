package com.vc.pacman.Model;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by chaahatjain on 17/9/18.
 */

public enum Environment {
    COIN,H_WALL,V_WALL,PELLET,FRUIT, NONE;

    /**
     * Used to draw all the environment on the board
     * @param canvas
     * @param cx
     * @param cy
     */
    public void draw(Canvas canvas, int cx, int cy, int square) {
        Paint paint = new Paint();
        switch (this) {
            case COIN:
                paint.setColor(Color.rgb(251,218,0));
                break;
            case FRUIT:
                paint.setColor(Color.rgb(65,229,101));
                break;
            case PELLET:
                paint.setColor(Color.rgb(39,127,249));
                break;
            case V_WALL:
                paint.setColor(Color.rgb(27,0,94));
                break;
            case H_WALL:
                paint.setColor(Color.rgb(27,0,94));
                break;
            default:
                paint.setColor(Color.BLACK);
        }
        canvas.drawRect(cx,cy,cx + square, cy + square, paint);
    }
}
