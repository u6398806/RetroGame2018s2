package com.vc.pacman.Model;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by chaahatjain on 17/9/18.
 * Represents the board on which game is played.
 */

public class Board implements PlayingBoard, Drawable {
    Environment[][] board = new Environment[WIDTH][HEIGHT];


    public Board(Environment[][] board) {
        this.board = board;
    }

    @Override
    public boolean checkLegalMove(Position entityPosition,Position movement) {
        Position position = entityPosition.add(movement);
        int x = position.getX();
        int y = position.getY();
        Environment typePresent = board[x][y];
        return (typePresent != Environment.H_WALL && typePresent != Environment.V_WALL);
    }

    @Override
    public Environment updateBoard(Position position) {
        int x = position.getX();
        int y = position.getY();
        Environment environment;
        environment = board[x][y];
        board[x][y] = Environment.NONE;
        return environment;
    }

    /**
     * Initialize a new board to play the game on.
     * @return
     */
     public static Environment[][] createNewBoard() {
        Environment[][] board = new Environment[23][26];
        Environment[] things = {Environment.COIN, Environment.FRUIT, Environment.PELLET};
        for (int j = 0; j < 26; j++) {
            Environment environment = things[j%3];
            for (int i = 0; i < 23; i++) {
                if (i == 0 || i == 22 || j == 0 || j == 25) {
                    board[i][j] = Environment.V_WALL;
                }
                else {
                    board[i][j] = environment;
                }
            }
        }
        return board;
    }

    @Override
    public Position getPixelLocation(Position position) {
        int x = BOARD_START_X + position.getX()*SQUARE_SIZE + SQUARE_SIZE/2;
        int y = BOARD_START_Y + position.getY()*SQUARE_SIZE + SQUARE_SIZE/2;
        return new Position(x,y);
    }

    /**
     * Draw a board.
     * @param canvas
     * @return
     */
    @Override
    public Canvas draw(Canvas canvas) {
        final int COL_LENGTH = BOARD_START_Y + HEIGHT*SQUARE_SIZE;
        final int ROW_LENGTH = BOARD_START_X + WIDTH*SQUARE_SIZE;
        Paint paint = new Paint();
        paint.setStrokeWidth(7);
        paint.setColor(Color.rgb(203,243,255));
        Environment environment;
        for (int i = 0; i < WIDTH; i++) {
            int cx = BOARD_START_X + i*SQUARE_SIZE;
            for (int j = 0; j < HEIGHT; j++) {
                int cy = BOARD_START_Y + j*SQUARE_SIZE;
               environment = board[i][j];
               environment.draw(canvas, cx, cy, SQUARE_SIZE);
            }
        }
        for (int i = 0; i < WIDTH + 1; i++) {
            int startX = BOARD_START_X + i*SQUARE_SIZE;
            canvas.drawLine(startX, BOARD_START_Y, startX, COL_LENGTH, paint);
        }
        for (int j = 0; j < HEIGHT + 1; j++) {
            int startY = BOARD_START_Y + j*SQUARE_SIZE;
            canvas.drawLine(BOARD_START_X, startY, ROW_LENGTH, startY, paint);
        }
        return canvas;
    }

}
