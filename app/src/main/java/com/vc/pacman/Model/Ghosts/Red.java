package com.vc.pacman.Model.Ghosts;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.vc.pacman.Model.Direction;
import com.vc.pacman.Model.Enemy;
import com.vc.pacman.Model.EnemyState;
import com.vc.pacman.Model.PacmanInterface;
import com.vc.pacman.Model.PlayingBoard;
import com.vc.pacman.Model.Position;

/**
 * Created by chaahatjain on 16/9/18.
 * go directly for pacman. Also take into account the direction this is facing
 */

public class Red extends Enemy {
    private Position targetTile = new Position(28,28);

    @Override
    public Canvas draw(Canvas canvas) {
        Position center = getCenter();
        int radius = getRadius();
        Paint paint = new Paint();
        if (this.getState() != EnemyState.FRIGHTENED) {
            paint.setColor(Color.rgb(214, 53, 43));
        }
        else {
            paint.setColor(Color.BLUE);
        }
        canvas.drawCircle(center.getX(), center.getY(), radius, paint);
        return canvas;
    }

    public Red(Position home, PacmanInterface pacman, PlayingBoard board) {
        super(home, pacman, board);
    }

    @Override
    public void applyAlgorithm(Position pacmanPosition) {
        super.applyAlgorithm(pacmanPosition);
        switch (getState()) {
            case CHASE: chase(pacmanPosition);
            break;
            case SCATTER: chase(targetTile);
            break;
            case FRIGHTENED:makeRandomMove();
            break;
        }
    }

}
