package com.vc.pacman.Model;

/**
 * Created by chaahatjain on 16/9/18.
 */

public enum Direction {
    LEFT,RIGHT,UP,DOWN,NONE;

    /**
     * Initialize the standard order array
     * @return
     */
    public static Direction[] createArray() {
        Direction[] directions = {UP,LEFT,DOWN,RIGHT};
        return directions;
    }

    /**
     * Get the position shift this move corresponds to
     * @param move
     * @return
     */
    public static Position getPosition(Direction move) {
        switch (move) {
            case UP: return new Position(0,1);
            case DOWN:return new Position(0,-1);
            case LEFT:return new Position(-1,0);
            case RIGHT:return new Position(1,0);
            case NONE: return new Position(0,0);
        }
        // same value as none
        return new Position(0,0);
    }


    @Override
    public String toString() {
        switch (this) {
            case LEFT: return "Left";
            case RIGHT: return "Right";
            case DOWN: return "Down";
            case UP: return "Up";
        }
        return "NONE";
    }


}
