package com.vc.pacman.Activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.vc.pacman.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.prefs.Preferences;

public class GameOverActivity extends Activity {
    Preferences prefs = Preferences.userNodeForPackage(GameOverActivity.class);
    private String COUNT = "COUNT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);
        int state = getIntent().getIntExtra("STATE",-1);
        int score = getIntent().getIntExtra("SCORE",-1);
        save(score);
        List<Integer> scores = load();
        Collections.sort(scores);
        Collections.reverse(scores);
        String notify = state == 0 ? "WIN" : "LOSE";
        TextView textView = findViewById(R.id.message);
        textView.setText("GAME OVER \nYOU " + notify + "\nYour Highest Score is " + scores.get(0));
    }

    public void playAgain(View v) {
        Intent intent = new Intent(this,GameActivity.class);
        startActivity(intent);
    }

    private List<Integer> load() {
        ArrayList<Integer> scores = new ArrayList<>();
        int count = prefs.getInt("COUNT",0);
        for (int i = 0; i < count; i++) {
            int score = prefs.getInt("" + i,0);
            scores.add(score);
        }
        return scores;
    }

    private void save(int score) {
        int count = prefs.getInt("COUNT",0);
        prefs.putInt("COUNT",count + 1);
        prefs.putInt("" + count, score);
    }
}
