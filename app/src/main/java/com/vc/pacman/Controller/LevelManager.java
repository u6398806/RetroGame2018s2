package com.vc.pacman.Controller;

import android.graphics.Canvas;

import com.vc.pacman.Model.Direction;
import com.vc.pacman.Model.GameState;
import com.vc.pacman.View.Level;

/**
 * Created by chaahatjain on 14/9/18.
 * Used to change from level A to level B
 */

public class LevelManager {
    Level level;
    public LevelManager() {
    }

    public void loadLevel() {
        this.level = new Level();
    }

    public GameState updateLevel() {

        return this.level.update();
    }

    public Canvas drawLevel(Canvas canvas) {
        return this.level.draw(canvas);
    }

    public void changePacmanDirection(Direction facing) {
        level.changePacmanDirection(facing);
    }

    public int getScore() {
        return level.getScore();
    }
}
