package com.vc.pacman.Controller;

import android.view.MotionEvent;
import android.view.View;

import com.vc.pacman.Model.Direction;

/**
 * Created by chaahatjain on 14/9/18.
 */

public class InputController implements View.OnTouchListener {
    LevelManager lm;
    private float startX,startY,endX,endY;
    public InputController(LevelManager lm) {
        this.lm = lm;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case (MotionEvent.ACTION_DOWN) :
                startX = motionEvent.getX();
                startY = motionEvent.getY();
                break;
            case MotionEvent.ACTION_UP :
                endX = motionEvent.getX();
                endY = motionEvent.getY();
                float distX = Math.abs(endX - startX);
                float distY = Math.abs(endY - startY);

                if (distX > distY) {
                    if (endX - startX > 20) {
                        // right
                        lm.changePacmanDirection(Direction.RIGHT);
                    } else {
                        // left
                        if (startX - endX > 20)
                        lm.changePacmanDirection(Direction.LEFT);
                    }
                }
                else {
                    if (endY - startY > 20) {
                        // up
                        lm.changePacmanDirection(Direction.UP);
                    }
                    else {
                        // down
                        if (startY - endY > 20) {
                            lm.changePacmanDirection(Direction.DOWN);
                        }
                    }
                }
                break;

        }
        return true;
    }


}
