package com.vc.pacman.View;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.vc.pacman.Model.Board;
import com.vc.pacman.Model.Controllable;
import com.vc.pacman.Model.Direction;
import com.vc.pacman.Model.Drawable;
import com.vc.pacman.Model.Enemy;
import com.vc.pacman.Model.Environment;
import com.vc.pacman.Model.GameState;
import com.vc.pacman.Model.Ghosts.*;
import com.vc.pacman.Model.Movable;
import com.vc.pacman.Model.Pacman;
import com.vc.pacman.Model.PacmanInterface;
import com.vc.pacman.Model.PlayingBoard;
import com.vc.pacman.Model.Position;

/**
 * Created by chaahatjain on 28/9/18.
 */

public class Level {
    // Each level has pacman, a board and the ghosts. Pacman and ghosts are constant, the only thing that changes is the board.
    PacmanInterface pacman;
    PlayingBoard board;

    public Level() {

        pacman = new Pacman();

        Environment[][] gameBoard = Board.createNewBoard();
        board = new Board(gameBoard);
        pacman.registerBoard(board);

        Position RedHome = new Position(12,13);
        Enemy Red = new Red(RedHome, pacman, board);

        Position BlueHome = new Position(12,14);
        Enemy Blue = new Blue(BlueHome, pacman, board);

        Position OrangeHome = new Position(11,14);
        Enemy Orange = new Orange(OrangeHome, pacman, board);

        Position PinkHome = new Position(10,14);
        Enemy Pink = new Pink(PinkHome, pacman, board);
        // Register all enemies as an observer to Pacman.
        pacman.addEnemy(Red);
        pacman.addEnemy(Blue);
        pacman.addEnemy(Orange);
        pacman.addEnemy(Pink);
    }

    /**
     * Update the level based on Pacman and Enemies.
     * @return
     */
    public GameState update() {
        ((Movable) pacman).move(); // Update pacman's position and the board
        pacman.sendPosition(); // update the ghosts position
        int score = ((Pacman) pacman).getScore();
        int numberOfLives = ((Pacman) pacman).getNumberOfLives();

        // For the win or lose criteria
        if (score > 15000 || numberOfLives <= 0) {
            GameState gameState = score > 15000 ? GameState.WIN : GameState.LOSE;
            return gameState;
        }
        return GameState.PLAYING;
    }

    /**
     * Draw the entire level
     * @param canvas
     * @return
     */
    public Canvas draw(Canvas canvas) {
        canvas = ((Drawable) pacman).draw(canvas);

        Paint textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(75);
        canvas.drawText("Swipe to Control", 200, 2000, textPaint);
        // Coin
        textPaint.setColor(Color.rgb(251,218,0));
        canvas.drawText("COIN",800,2000, textPaint);
        // Fruit
        textPaint.setColor(Color.rgb(65,229,101));
        canvas.drawText("FRUIT",200,2150,textPaint);
        // Pellet
        textPaint.setColor(Color.rgb(39,127,249));
        canvas.drawText("PELLET",500,2150,textPaint);
        // Wall
        textPaint.setColor(Color.rgb(27,0,94));
        canvas.drawText("WALL",800,2150,textPaint);

        return canvas;
    }

    /**
     * Change the direction Pacman is facing based on swipe events
     * @param direction
     */
    public void changePacmanDirection(Direction direction) {
        ((Controllable)pacman).changeDirectionFacing(direction);
    }

    public int getScore() {
        return ((Pacman) pacman).getScore();
    }
}
