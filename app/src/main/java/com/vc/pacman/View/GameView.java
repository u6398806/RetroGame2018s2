package com.vc.pacman.View;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.vc.pacman.Activities.GameOverActivity;
import com.vc.pacman.Controller.InputController;
import com.vc.pacman.Controller.LevelManager;
import com.vc.pacman.Model.GameState;

/**
 * Created by chaahatjain on 14/9/18.
 */

public class GameView extends SurfaceView implements Runnable {
    // declare all the variables here
    Thread gameThread;
    private volatile boolean running;
    InputController ic;
    LevelManager lm;

    SurfaceHolder ourHolder;
    Canvas canvas;

    // Till here all declarations
    public GameView(Context context) {
        super(context);
        lm = new LevelManager();
        ic = new InputController(lm);
        this.setOnTouchListener(ic);
        lm.loadLevel();
        ourHolder = getHolder();
    }

    @Override
    public void run() {
        while(running) {
            update();
            draw();
            control();
        }
    }

    private void update() {

        GameState gameState = lm.updateLevel();
        if (gameState != GameState.PLAYING) {
            Intent intent = new Intent(this.getContext(), GameOverActivity.class);
            int state = gameState == GameState.WIN ? 0 : 1;
            intent.putExtra("STATE",state);
            intent.putExtra("SCORE",lm.getScore());
            this.getContext().startActivity(intent);
        }
    }

    private void draw() {
        // drawing level. lm.drawLevel();
        if (ourHolder.getSurface().isValid()) {
            canvas = ourHolder.lockCanvas();

            // clear the canvas
            canvas.drawColor(Color.BLACK);

            lm.drawLevel(canvas);
            ourHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void control() {
        try {
            gameThread.sleep(500);
        } catch (InterruptedException e) {
            System.out.println("Failed in control");
        }
    }

    public void pause() {
        running = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            System.out.println("Failed in pause");
        }
    }

    public void resume() {
        running = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}
