package com.vc.pacman;

import com.vc.pacman.Model.Position;
import com.vc.pacman.Model.PositionOperations;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by chaahatjain on 17/9/18.
 * This library tests whether all the functions in Position work correctly
 */
public class PositionTest {
    /**
     * Check whether the equals function works properly
     */
    @Test
    public void equalsTest() {
        PositionOperations position = new Position(5,5);
        PositionOperations position1 = new Position(5,5);
        assertEquals(position,position1);
        assertTrue(position.equals(position1));
    }

    /**
     * Checks whether the add function works properly
     */
    @Test
    public void addTest() {
        Position position = new Position(5,5);
        Position position1 = new Position(1,1);
        Position position2 = new Position(6,6);
        assertEquals(position.add(position1),position2);
    }

    /**
     * Checks whether the distance function works properly
     */
    @Test
    public void distanceTest() {
        Position position = new Position(13,5);
        Position position1 = new Position(10,7);
        Position position2 = new Position(-3,2);
        assertEquals(position.distanceFrom(position1),position2);
    }

    /**
     * Test whether the scaleBy function works for unit vectors
     */
    @Test
    public void scalarTest() {
        Position position = new Position(1,0);
        Position position1 = new Position(0,1);
        Position position2 = new Position(-1,0);
        Position position3 = new Position(0,-1);
        int scalar = 2;

        assertEquals(new Position(2,0), position.scaleBy(scalar));
        assertEquals(new Position(0,2), position1.scaleBy(scalar));
        assertEquals(new Position(-2,0), position2.scaleBy(scalar));
        assertEquals(new Position(0,-2), position3.scaleBy(scalar));
    }

    @Test
    public void imageTest() {
        Position red = new Position(0,0);

        Position pivot = new Position(2,0);
        assertEquals(new Position(4,0), red.image(pivot)); // along the y axis

        pivot = new Position(0,2);
        assertEquals(new Position(0,4), red.image(pivot)); // along the y axis

        pivot = new Position(2,2);
        assertEquals(new Position(4,4), red.image(pivot)); // along y = x

        red = new Position(12,6);
        pivot = new Position(13,5);
        assertEquals(new Position(14,4), red.image(pivot)); // random line

        red = new Position(12,6);
        pivot = new Position(15,20);
        assertEquals(new Position(18,34), red.image(pivot));
    }
}