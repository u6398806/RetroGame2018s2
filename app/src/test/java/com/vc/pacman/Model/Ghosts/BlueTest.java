package com.vc.pacman.Model.Ghosts;

import com.vc.pacman.Model.Board;
import com.vc.pacman.Model.Direction;
import com.vc.pacman.Model.Enemy;
import com.vc.pacman.Model.EnemyFunctions;
import com.vc.pacman.Model.Environment;
import com.vc.pacman.Model.Pacman;
import com.vc.pacman.Model.Position;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by chaahatjain on 17/9/18.
 * these tests are used to check whether or not the algorithm is applied correctly for the blue enemy
 */
public class BlueTest {
    private Environment[][] getboard(){
        Environment[][] board = new Environment[23][26]; // on this board every move is legal
        for (int i = 0; i < 23; i++) {
            for (int j = 0; j < 26; j++) {
                board[i][j] = Environment.COIN;
            }
        }
        return board;
    }

    /**
     * This test checks whether the enemy moves towards pacman if no obstacles are there
     */
    @Test
    public void algoForAllLegal() {
        Board mBoard = new Board(getboard());
        Position home = new Position(8,4);
        Pacman pacman = new Pacman(); // Standard home at 13,5
        Enemy enemy = new Blue(home,pacman,mBoard);
        pacman.addEnemy(enemy);
        home = new Position(14,7);
        Enemy enemy1 = new Red(home, pacman, mBoard);
        pacman.addEnemy(enemy1);
        pacman.sendPosition();
        assertEquals (new Position(8,3), enemy.position);
        pacman.sendPosition();
        assertEquals(new Position(9,3), enemy.position);
    }

}