package com.vc.pacman.Model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by chaahatjain on 27/9/18.
 */
public class PacmanTest {

    private Environment[][] createBoard() {
        Environment[][] board = new Environment[23][26];
        Environment[] things = {Environment.COIN, Environment.FRUIT, Environment.PELLET};
        for (int j = 0; j < 26; j++) {
            Environment environment = things[j%3];
            for (int i = 0; i < 23; i++) {
                board[i][j] = environment;
            }
        }
        return board;
    }

    @Test
    public void IncreaseScore() throws Exception {
        Pacman pacman = new Pacman(); // Position is 13,5. Direction facing is left
        Environment[][] board = createBoard();
        Board board1 = new Board(board);
        pacman.registerBoard(board1);

        // Eating a pellet
        pacman.move();
        assertEquals(true, pacman.isPellet());

        pacman.changeDirectionFacing(Direction.DOWN);
        // Eating a fruit
        pacman.move();
        assertEquals(500, pacman.getScore());

        // Eating a coin
        pacman.move();
        assertEquals(600,pacman.getScore());

        // Updating the board correctly
        pacman.changeDirectionFacing(Direction.UP);
        pacman.move();
        assertEquals(600, pacman.getScore());
    }
}