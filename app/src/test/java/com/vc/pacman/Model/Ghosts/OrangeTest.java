package com.vc.pacman.Model.Ghosts;

import com.vc.pacman.Model.Board;
import com.vc.pacman.Model.Direction;
import com.vc.pacman.Model.Enemy;
import com.vc.pacman.Model.EnemyFunctions;
import com.vc.pacman.Model.Environment;
import com.vc.pacman.Model.Pacman;
import com.vc.pacman.Model.Position;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by chaahatjain on 22/9/18.
 */
public class OrangeTest {
    private Environment[][] getboard(){
        Environment[][] board = new Environment[23][26]; // on this board every move is legal
        for (int i = 0; i < 23; i++) {
            for (int j = 0; j < 26; j++) {
                board[i][j] = Environment.COIN;
            }
        }
        return board;
    }

    /**
     * This test considers 2 orange entities. One close to pacman (< 8 tiles) and the other far from it.
     * We expect that the first one moves away from pacman while the latter approaches him.
     */
    @Test
    public void algoForAllLegal() {
        Board mBoard = new Board(getboard());
        Position home = new Position(12,4);
        Pacman pacman = new Pacman(); // Standard home at 13,5
        Enemy enemy = new Orange(home,pacman,mBoard);
        Position home2 = new Position(8,2);
        Enemy enemy1 = new Orange(home2,pacman,mBoard);
        pacman.addEnemy(enemy);
        pacman.addEnemy(enemy1);
        pacman.sendPosition();
        assertEquals (new Position(11,4),enemy.position);
        assertEquals(new Position(8,3), enemy1.position);
    }

}