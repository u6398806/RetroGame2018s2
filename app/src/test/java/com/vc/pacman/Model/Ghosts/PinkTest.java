package com.vc.pacman.Model.Ghosts;

import com.vc.pacman.Model.Board;
import com.vc.pacman.Model.Direction;
import com.vc.pacman.Model.Enemy;
import com.vc.pacman.Model.EnemyFunctions;
import com.vc.pacman.Model.Environment;
import com.vc.pacman.Model.Pacman;
import com.vc.pacman.Model.Position;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by chaahatjain on 17/9/18.
 */
public class PinkTest {
    private Environment[][] getboard(){
        Environment[][] board = new Environment[23][26]; // on this board every move is legal
        for (int i = 0; i < 23; i++) {
            for (int j = 0; j < 26; j++) {
                board[i][j] = Environment.COIN;
            }
        }
        return board;
    }

    @Test
    public void algoForAllLegal() {
        Board mBoard = new Board(getboard());
        Position home = new Position(13,6);
        Pacman pacman = new Pacman(); // Standard home at 13,5
        Enemy enemy = new Pink(home,pacman,mBoard);
        pacman.addEnemy(enemy);
        pacman.sendPosition();
        assertEquals (new Position(12,6), enemy.position); // move left where pacman is headed rather than going down where he can be captured
    }

    /**
     * This test checks whether the enemy moves close to pacman when blocked on highest priority side
     */
    @Test
    public void algoForBlocked() {
        Environment[][] board = getboard();
        board[12][5] = Environment.V_WALL;
        Board mBoard = new Board(board);
        Position home = new Position(12,4);
        Pacman pacman = new Pacman();
        Enemy enemy = new Pink(home,pacman,mBoard);
        pacman.addEnemy(enemy);
        pacman.sendPosition();
        assertEquals (enemy.position,new Position(11,4));
    }

    /**
     * This algorithm checks whether the opposite direction condition is implemented correctly
     */
    @Test
    public void algoOppDirection() {
        Environment[][] board = getboard();

        // restrict the board
        board[12][4]  = Environment.V_WALL;
        board[11][5] = Environment.H_WALL;
        board[10][3] = Environment.V_WALL;
        board[12][3] = Environment.V_WALL;
        board[10][4] = Environment.V_WALL;

        Board mBoard = new Board(board);
        Position home = new Position(11,4);
        Pacman pacman = new Pacman();
        Enemy enemy = new Pink(home,pacman,mBoard);

        pacman.addEnemy(enemy);
        pacman.sendPosition();
        assertEquals(enemy.position,new Position(11,3)); // force it to go down

        pacman.sendPosition();
        assertNotEquals(enemy.position,new Position(11,4)); // as it cannot move back
        assertEquals(enemy.position,new Position(11,2)); // only option

        // block off all three walls where it can go
        board[10][2] = Environment.V_WALL;
        board[12][2] = Environment.V_WALL;
        board[11][1] = Environment.H_WALL;
        pacman.sendPosition();
        assertNotEquals(enemy.position, new Position(11,3)); // does not go up
        assertEquals(enemy.position, new Position(11,2)); // should not move
    }
}